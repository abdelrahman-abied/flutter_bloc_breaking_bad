import '../api_services/characters_service.dart';
import '../model/character.dart';
import '../model/quotes.dart';

class CharactersRepository {
  final CharachtersService charactersService;

  CharactersRepository(this.charactersService);
  Future<List<Character>> getAllCharacters() async {
    final cah = await charactersService.getAllCharacters();
    // print("ccccccccccccc${cah.length}");
    return cah;
  }

  Future<List<Quote>> getCharacterQuote(String characterName) async {
    final quotes = await charactersService.getCharacterQuotes(characterName);
    // print("ccccccccccccc${cah.length}");
    return quotes.map((quote) => Quote.fromJson(quote)).toList();
  }
}
