part of 'characterquote_cubit.dart';

@immutable
abstract class CharacterQuoteState {}

class CharacterQuoteInitial extends CharacterQuoteState {}

class CharacterQuoteLoaded extends CharacterQuoteState {
  final List<Quote> characterQuote;

  CharacterQuoteLoaded(this.characterQuote);
}
