import 'package:bloc/bloc.dart';
import '../../data/model/quotes.dart';
import '../../data/repository/characters_repository.dart';
import 'package:meta/meta.dart';

part 'characterquote_state.dart';

class CharacterQuoteCubit extends Cubit<CharacterQuoteState> {
  final CharactersRepository CharactersRepo;
  List<Quote> quotes = [];
  CharacterQuoteCubit(this.CharactersRepo) : super(CharacterQuoteInitial());

  void getCharacterQuotes(String characterName) {
    CharactersRepo.getCharacterQuote(characterName).then((quotes) {
      emit(CharacterQuoteLoaded(quotes));
    });
  }
}
