import '../data/model/character.dart';
import '../screens/character_details.dart';
import '../utils/constants.dart';
import '../utils/style.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class CharacterItem extends StatelessWidget {
  Character character;

  CharacterItem({Key? key, required this.character}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      child: Container(
        width: double.infinity,
        margin: const EdgeInsetsDirectional.fromSTEB(5, 5, 5, 5),
        padding: const EdgeInsetsDirectional.all(4),
        decoration: BoxDecoration(
          color: accentColor,
          borderRadius: BorderRadius.circular(8),
        ),
        child: InkWell(
          onTap: () => Navigator.pushNamed(
              context, Constants.characterDetailsScreen,
              arguments: character),
          child: GridTile(
            child: Hero(
              tag: character.charId!,
              child: Container(
                color: primaryColor,
                child: CachedNetworkImage(
                  imageUrl: character.img!,
                  fit: BoxFit.cover,
                  errorWidget: (context, _, error) => Image.network(
                    "https://www.usmagazine.com/wp-content/uploads/2020/08/Breaking-Bad-Where-Are-They-Now.jpg?w=1200&quality=86&strip=all",
                    fit: BoxFit.fill,
                  ),
                  placeholder: (context, _) => const Center(
                    child: SpinKitRipple(
                      color: secondaryColor,
                    ),
                  ),
                ),
              ),
            ),
            footer: Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              color: Colors.black54,
              alignment: Alignment.bottomCenter,
              child: Text(
                character.name.toString(),
                style: const TextStyle(
                  height: 1.3,
                  fontSize: 16,
                  color: accentColor,
                  fontWeight: FontWeight.bold,
                ),
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
