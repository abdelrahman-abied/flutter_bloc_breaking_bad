import '../business_logic/cubit/characterquote_cubit.dart';
import '../business_logic/cubit/characters_cubit.dart';
import '../data/api_services/characters_service.dart';
import '../data/model/character.dart';
import '../data/repository/characters_repository.dart';
import '../screens/character_details.dart';
import '../screens/characters.dart';
import 'constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppRouter {
  late CharactersRepository _charactersRepository;
  late CharactersCubit _characterCubit;
  late CharacterQuoteCubit _characterQuoteCubit;

  AppRouter() {
    _charactersRepository = CharactersRepository(CharachtersService());
    _characterCubit = CharactersCubit(_charactersRepository);
    _characterQuoteCubit = CharacterQuoteCubit(_charactersRepository);
  }
  Route? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Constants.charactersScreen:
        return MaterialPageRoute(
          builder: (_) => BlocProvider(
            create: (BuildContext context) => _characterCubit,
            child: CharactersScreen(),
          ),
        );
      case Constants.characterDetailsScreen:
        final character = settings.arguments as Character;
        return MaterialPageRoute(
          builder: (BuildContext context) => BlocProvider(
            create: (_) => _characterQuoteCubit
              ..getCharacterQuotes(character.name.toString()),
            child: CharacterDetailsScreen(character: character),
          ),
        );
    }
  }
}
